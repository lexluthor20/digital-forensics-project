iosimple is a tool that allows anyone with access to unencrypted iphone backups
to view them in human-readable format.

It also cracks restrictions pins in 3-5 seconds and places the pin number in
the file RESTRICTIONS_PASSWORD.txt

For each backup, a folder is created with the name iosimple-xxx, where the
xxx is the time that the backup was created.


Mac Installation
-------------------------------------------------------------------------------

1.) Install `brew` (https://brew.sh/ and https://www.howtogeek.com/211541/homebrew-for-os-x-easily-installs-desktop-apps-and-terminal-utilities/):

    xcode-select --install

    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

    brew doctor

    brew install caskroom/cask/brew-cask

2.) Install `python3` (and ensure `pip` is installed):

    brew install python3
    
    pip install --upgrade pip

3.) Install iTunes and create a backup of your iPhone.

4.) Clone this repository under your home directory (`~`), `cd` into the repository, then run:

    python3 -m pip install -r dependencies.txt

    python3 iosimple.py


Linux Installation
-------------------------------------------------------------------------------

Additional testing on Linux required, but I would try getting iTunes for Windows and then running it in Wine.

Windows Installation
-------------------------------------------------------------------------------

1.) Install `python3` (https://www.python.org/downloads/windows/)

    Make sure to click the add python to PATH checkbox when installing. Allows you to skip steps 2, 3 and 4.

    Make sure to click the Disable path length limit button. Not clicking this will break the program.

2.) Open the following folder in `file explorer`
    
    %USERPROFILE%\AppData\Local\Programs\Python\

3.) Write down the Folder name in this folder (e.g. Python37)

4.) Add the following To your PATH system variable (to make `python.exe` available on the command-line):

    %USERPROFILE%\AppData\Local\Programs\Python\Python37\

    Note: Use the python version that you have (Python37 in my case)

    Also Note: This step can be skipped if you clicked the Add to PATH checkbox when installing python for windows

5.) Install Visual Studios C++ Build Tools: (http://aka.ms/BuildTools)

6.) Install iTunes (https://www.apple.com/itunes/download/), make a backup of your iphone and add the following To your PATH system variable: 

    C:\Program Files (x86)\Common Files\Apple\Apple Application Support\

7.) Open up `cmd.exe`, `CD` into `digital-forensics-project/`, and run the following:

    python -m pip install -r win_dependencies.txt

    python iosimple.py
