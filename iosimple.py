import os
import sqlite3
import glob
import sys
import xml.etree.ElementTree as etree
import magic
from shutil import copyfile
from shutil import rmtree
import subprocess
import base64
from passlib.utils.pbkdf2 import pbkdf2
from time import time
import platform
from pathlib import Path
import magic

macos = "Darwin"
win = "Windows"
repo = os.getcwd() + '/'

#detemining the backup_path depending on the OS
if platform.system() == macos:
    backup_path = Path('/Users/Home/Library/Application Support/MobileSync/Backup/')
else:
    user = os.getlogin()
    backup_path = 'C:/Users/'+user+'/AppData/Roaming/Apple Computer/MobileSync/Backup'


### RECOVERING RESTRICTIONS PASSWORD ###
def restrictions_password(b_folder):
    print("Cracking Restrictions Password")
    password = []

    iphone_dir = repo+b_folder+'/IPHONE_FILES/'
    home_dir = repo+b_folder+'/'

    try:
        #getting hash and salt
        tree = etree.parse(os.path.join(iphone_dir,'HomeDomain/Library/Preferences/com.apple.restrictionspassword.plist'))
        for data in tree.iter('data'):
            password.append(''.join(data.text).strip("\n\t"))
        crack(password[0],password[1], b_folder)

    except FileNotFoundError:
        print("Restrictions password file not found.\n")
        print("Either there is no restrictions password set or it was moved.\n")

### This function taken from:
### https://rstforums.com/forum/topic/107395-iosrestrictionbruteforce-crack-ios-restriction-passcodes-with-python/
### updated print calls to write to a file
def crack(secret64, salt64, b_folder):

    iphone_dir = repo+b_folder+'/IPHONE_FILES/'
    home_dir = repo+b_folder+'/'

    f = open(os.path.join(home_dir,'restrictions_password.txt'), 'w')
    f.write("secret: ")
    f.write(secret64)
    f.write("\nsalt: ")
    f.write(salt64)
    secret = base64.b64decode(secret64)
    salt = base64.b64decode(salt64)
    start_t = time()
    for i in range(10000):
        key = "%04d" % (i)
        out = pbkdf2(key, salt, 1000)
        if out == secret:
            f.write("\nkey: ")
            f.write(key)
            duration = time() - start_t
            f.write("\n%f seconds" % (duration))
    f.close()

def init_analysis():
    list = os.listdir(backup_path)
    backups = []
    #print(sqlite3.version)
    #add the backups to the list of backup files
    for x in list:
        pathname = os.path.join(backup_path,Path(x))
        if os.path.isdir(pathname):
            backups.append(pathname)

    for backup in backups:
        files = glob.glob(backup+'/**', recursive = False)
        found = False

        #connecting to database
        for x in files:
            if "Manifest.db" in x:
                #print(x)
                conn = sqlite3.connect(x)
                found = True
                break

        if found == False:
            print("Error finding manifest database")
            sys.exit()

        #setting the directory name to the time of backup
        tree = etree.parse(backup+"/Info.plist")
        found = False
        for tag in tree.iter():
            #getting the element after "Last Backup Date"
            if found == True:
                #print(tag.text)
                found = False
                #Windows doesn't allow ':' in their folder names, so
                #remove time of backup creation for windows machines
                if platform.system() == macos:
                    backup_folder = "iosimple-" + tag.text
                else:
                    backup_folder = "iosimple-" + tag.text[:10]
                print(backup_folder)
                break
            #finding the element before the actual date
            if "Last Backup Date" == tag.text:
                #print(tag.text)
                #print(tag.attrib)
                #print(tag.tag)
                #print(tag.tail)
                found = True

        iphone_dir = repo+backup_folder+'/IPHONE_FILES/'
        home_dir = repo+backup_folder+'/'

        #building filesystem
        fileID = 0
        domain = 1
        relativePath = 2
        flags = 3
        File = 4
        levels = 0
        untitled_number = 0
        mediatype = ['JPEG','EXIF','TIFF','GIF','BMP','PPM','PGM','PBM','PNM', 'PNG'
            'WebP','HEIF','BAT','BPG', 'PDF', 'WebM', 'MATROSKA', 'FLV', 'F4V', 'VOB',
            'OGG', 'DIRAC', 'ISO', 'MOV', 'AVI', 'QT', 'MPEG', 'MPEG-4', 'M4V']
        if(os.path.exists(backup_folder)):
            print("This backup has already been created.\n")
            print("Please delete this folder and try again")
            sys.exit()
        else:
            #print(os.getcwd())
            os.mkdir(backup_folder)

        os.chdir(Path(backup_folder))
        os.mkdir("IPHONE_FILES")

        os.mkdir("MEDIA")
        os.chdir("IPHONE_FILES")

        dir_length = 0
        #making domain folders
        try:
            c = conn.cursor()
            for row in c.execute("SELECT domain FROM files"):
                if not os.path.exists(row[0]):
                    os.mkdir(row[0])
            #making relative path folders and placing the files in them
            for row in c.execute("SELECT * FROM files"):
                #checking the domain folder exists
                if not os.path.exists(row[domain]):
                    print("Fatal error ocurred making the backup")
                else:
                    #entering correct domain folder
                    os.chdir(row[domain])
                    subdirs = row[relativePath].split("/")
                    dir_length = len(subdirs)
                    index = 0
                    #making subdirs
                    while(len(subdirs) > 1 and subdirs != ['']):
                        if not os.path.exists(subdirs[0]):
                            os.mkdir(subdirs[0])
                        os.chdir(subdirs[0])
                        subdirs.pop(0)
                    try:
                        #moving file from backup into file structure
                        copyfile((str(backup)+'/'+str(row[fileID][:2])+'/'+str(row[fileID])),subdirs[0])
                        #converting Apple binary property lists to xml
                        if magic.from_file(subdirs[0])  == "Apple binary property list":
                            subprocess.call(["plutil", "-convert", "xml1", subdirs[0]])
                        elif magic.from_file(subdirs[0]).split(" ")[0] in mediatype:
                            copyfile(subdirs[0],os.path.join(home_dir,'MEDIA/',subdirs[0]))
                    except (FileNotFoundError, OSError, magic.MagicException):
                        pass
                #going back up to the iphone files directory
                os.chdir(Path(iphone_dir))
        except sqlite3.DatabaseError:
            print("ERROR ENTERING BACKUP.\nEITHER THE BACKUP IS ENCRYPTED OR")
            print("THERE IS A NON-BACKUP FOLDER IN THE BACKUP FOLDER.\n")
            print("TRYING NEXT BACKUP IF THERE IS ONE.\n")
            os.chdir('/Users/Home/digital-forensics-project/')
            rmtree(backup_folder)
            continue

        print("END OF INITIAL ANALYSIS FOR BACKUP\n")
        restrictions_password(backup_folder)

#print(os.getlogin())
#print(backup_path)
init_analysis()
